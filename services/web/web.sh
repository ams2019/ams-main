#!/bin/bash

# LOG
LOG=/home/pi/ams-main/logs/web-client.log
truncate -s 0 $LOG

# SETUP
source /home/pi/catkin_ws/devel/setup.bash

# WEB CLIENT
unbuffer rosrun ams-main web_client.py 2>&1 | tee $LOG
