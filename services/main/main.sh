#!/bin/bash

# LOG
LOG=/home/pi/ams-main/logs/main.log
truncate -s 0 $LOG

# SETUP
source /home/pi/catkin_ws/devel/setup.bash

# MAIN
unbuffer roslaunch ams-main main.launch 2>&1 | tee $LOG
