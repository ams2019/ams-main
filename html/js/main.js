var lastTopicUpdate = new Date();
var wsConnected = false;

setInterval(function () {
    getMap();
}, 5000)

var framesPerSecond = 20;
 
function animate() {
    setTimeout(function() {
        if(map != null){
            drawMap();
        }
        requestAnimationFrame(animate);
    }, 1000 / framesPerSecond);
}

animate();

var ws = new WebSocket("ws://" + window.location.hostname + ":8000");
ws.onopen = function () {
    wsConnected = true;
    getMap();
}
ws.onclose = function () {
    wsConnected = false;
}
ws.onerror = function () {
    wsConnected = false;
}
ws.onmessage = function (event) {

    obj = JSON.parse(event.data);

    if (obj.type == $("#selectTopic").val()) {
        let html = "";
        for (var k in obj.data) {
            html += "<tr><td style=\"text-align: left;\">" + k + "</td><td style=\"text-align: center;\">" + obj.data[k] + "</td></tr>";
        }
        $("#tableTopic").html(html);
        lastTopicUpdate = new Date();

    } else if (obj.type == "map"){
        map = obj;
        canvas.height = map.data.height * canvas.width/map.data.width;
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        if(canvas.width<map.data.width) 
            ctx.scale(canvas.width/map.data.width, canvas.width/map.data.width);
    } else if (obj.type == "pose"){
        pose = obj.data;
        if(obj.data.is_flying){
            droneState = 1;
        } else {
            droneState = 2;
        }
    } else if (obj.type == "path"){
        path = obj.data.points;
    } else if (obj.type == "take_off"){
        droneState = 1;
    } else if (obj.type == "landing"){
        droneState = 2;
    }
}

var droneState = 0;

var canvas = document.getElementById("map_canvas");
var ctx = canvas.getContext('2d');
ctx.save();
var map = null;
var path = null;
var pose = {x: -10, y: -10};

function  getMousePos(evt) {
    rect = canvas.getBoundingClientRect(), 
    scaleX = canvas.width / rect.width,    
    scaleY = canvas.height / rect.height; 
    return {
      x: (evt.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
      y: (evt.clientY - rect.top) * scaleY    // been adjusted to be relative to element
    }
}

var target = null;

canvas.addEventListener("click", function(evt){ 
    if(map != null){
        mousePos = getMousePos(evt);
        realPos = {
            x: (-origin.x + (mousePos.x))*map.data.resolution,
            y: (map.data.height-origin.y - (mousePos.y))*map.data.resolution
        };

        target = realPos;

        obj = new Object();
        obj.type = "target";
        obj.data = new Object();
        obj.data.x = realPos.x;
        obj.data.y = realPos.y;
        ws.send(JSON.stringify(obj));
    }
});

var rotorAngle = 0;

function drawMap(){
    origin = {x: -map.data.originX, y: -map.data.originY};
    ctx.clearRect(0,0,canvas.width,canvas.height);
    for(y = 0; y < map.data.height; y++){
        for(x = 0; x < map.data.width; x++){
            if(map.data.points[y*map.data.width + x] == -1){
                ctx.fillStyle = "#777";
                ctx.fillRect(x,map.data.height-1-y,1,1);
            } else if(map.data.points[y*map.data.width + x] > 50){
                ctx.fillStyle = "#000";
                ctx.fillRect(x,map.data.height-y,1,1);
            }
        }
    }
    ctx.closePath();

    
    // path
    if (path != null){
        ctx.beginPath();
        ctx.fillStyle = "black";
        for(i = 0; i < path.length; i += 5){
            ctx.fillRect(origin.x+path[i][0]/map.data.resolution,map.data.height-origin.y-path[i][1]/map.data.resolution,1,1);
        }
        ctx.closePath();
    }

    // target
    if(target != null){
        ctx.beginPath();
        ctx.fillStyle = "green";
        ctx.fillRect(origin.x-2+target.x/map.data.resolution,map.data.height-origin.y-2-target.y/map.data.resolution, 4, 4);
        ctx.closePath();
    }

    // drone body
    droneX = origin.x+(pose.x/map.data.resolution);
    droneY = map.data.height-origin.y-pose.y/map.data.resolution;
    ctx.beginPath();
    ctx.fillStyle = "black";
    ctx.fillRect(droneX-4,droneY-4,8,8);
    ctx.closePath();
    ctx.beginPath();
    ctx.strokeStyle = "black";
    ctx.moveTo(droneX-7, droneY-7);
    ctx.lineTo(droneX+7, droneY+7);
    ctx.moveTo(droneX+7, droneY-7);
    ctx.lineTo(droneX-7, droneY+7);
    ctx.stroke();
    ctx.closePath();

    // drone rotors

    ctx.beginPath();
    ctx.arc(droneX+7, droneY+7,4,0, Math.PI*2);
    ctx.stroke();
    ctx.closePath();
    ctx.beginPath();
    ctx.arc(droneX-7, droneY+7,4,0, Math.PI*2);
    ctx.stroke();
    ctx.closePath();
    ctx.beginPath();
    ctx.arc(droneX+7, droneY-7,4,0, Math.PI*2);
    ctx.stroke();
    ctx.closePath();
    ctx.beginPath();
    ctx.arc(droneX-7, droneY-7,4,0, Math.PI*2);
    ctx.stroke();
    ctx.closePath();

    ctx.beginPath();
    ctx.strokeStyle = "black";
    rotorCenterX = droneX-7
    rotorCenterY = droneY-7
    ctx.moveTo(rotorCenterX-(Math.cos(rotorAngle)*3), rotorCenterY-(Math.sin(rotorAngle)*3));
    ctx.lineTo(rotorCenterX+(Math.cos(rotorAngle)*3), rotorCenterY+(Math.sin(rotorAngle)*3));
    rotorCenterX = droneX+7
    rotorCenterY = droneY-7
    ctx.moveTo(rotorCenterX-(Math.cos(rotorAngle)*3), rotorCenterY-(Math.sin(rotorAngle)*3));
    ctx.lineTo(rotorCenterX+(Math.cos(rotorAngle)*3), rotorCenterY+(Math.sin(rotorAngle)*3));
    rotorCenterX = droneX+7
    rotorCenterY = droneY+7
    ctx.moveTo(rotorCenterX-(Math.cos(rotorAngle)*3), rotorCenterY-(Math.sin(rotorAngle)*3));
    ctx.lineTo(rotorCenterX+(Math.cos(rotorAngle)*3), rotorCenterY+(Math.sin(rotorAngle)*3));
    rotorCenterX = droneX-7
    rotorCenterY = droneY+7
    ctx.moveTo(rotorCenterX-(Math.cos(rotorAngle)*3), rotorCenterY-(Math.sin(rotorAngle)*3));
    ctx.lineTo(rotorCenterX+(Math.cos(rotorAngle)*3), rotorCenterY+(Math.sin(rotorAngle)*3));
    ctx.stroke();
    ctx.closePath();
    if(droneState == 1){
        rotorAngle += Math.PI / 6;
    }
}

function resetMap() {
    obj = new Object();
    obj.type = "reset_sim";
    ws.send(JSON.stringify(obj));
}

function takeOff(){
    obj = new Object();
    obj.type = "take_off";
    ws.send(JSON.stringify(obj));
}

function landing(){
    obj = new Object();
    obj.type = "landing";
    ws.send(JSON.stringify(obj));
}

function getMap(){
    obj = new Object();
    obj.type = "get_map";
    ws.send(JSON.stringify(obj));
}
