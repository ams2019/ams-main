# AMS Main

This repository contains all code used for mapping, calculations and controls of the drone.

## Requirements

1. Setup rosflight (cf. [Rosflight Docs](https://docs.rosflight.org/user-guide/gazebo_simulation/))

        cd ~/catkin_ws/src
        git clone https://github.com/rosflight/rosflight.git
        cd rosflight
        git submodule update --init --recursive
        cd ~/catkin_ws
        catkin_make

2. Install hector slam packages

        sudo apt install ros-"$(rosversion -d)"-hector-mapping -y
        sudo apt install ros-"$(rosversion -d)"-hector-imu-attitude-to-tf -y

3. Install ROS navigation packages

        sudo apt install ros-"$(rosversion -d)"-navigation -y

    In case there is an `unmet dependency error` try installing with aptitude

        sudo apt-get install aptitude -y
        sudo aptitude install ros-"$(rosversion -d)"-navigation

4. Install Move-Base for Cost Map

        sudo apt install ros-"$(rosversion -d)"-move-base -y

5. Install Navigation Messages

        cd ~/catkin_ws/src
        git clone https://github.com/ros-planning/navigation_msgs.git
        cd ~/catkin_ws
        catkin_make

6. Install Simple Websocket Server

        pip install simple_websocket_server

## Installation

1. Clone `ams-main` into home directory

        cd ~
        git clone https://Rafael91@bitbucket.org/ams2019/ams-main.git

2. Symlink `ams-main` folder into catkin workspace

        sudo ln -s ~/ams-main ~/catkin_ws/src/ams-main

3. Enter catkin workspace and build

        cd ~/catkin_ws
        catkin_make

### Register as Linux-Service

To register `main` and `web` as linux service, link the following files:  
(This adds a symlinks at `/etc/systemd/system`)

    sudo systemctl link ~/ams-main/services/main/main.service
    sudo systemctl link ~/ams-main/services/web/web.service

Start/Stop/Restart service with

    sudo service {service-name} {start/stop/restart}

After making changes on service-file restart `systemctl` with

    sudo systemctl daemon-reload

Then restart service with

    sudo service {service-name} restart

## How to use

Without service (separate terminals):

    roslaunch ams-main main.launch
    rosrun ams-main web-client.py

As service:

    sudo service main start
    sudo service web start

Open Web UI in Browser:  
(If Port 80 is already in use, change port in `ams-main/src/web_client.py`)

    http://localhost

When running the web-server on the drone, exchange `localhost` with the IP-address of the companion computer to access the Web UI via Network.

## Logs

All services writes logs to the `logs` folder. To get the output of all logfiles run the monitor script

    ~/ams-main/logs/monitor.sh
