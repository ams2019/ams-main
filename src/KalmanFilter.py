#!/usr/bin/env python

class KalmanFilter:
    def __init__(self, init_val):
        self.measurement_sig = 1000.
        self.motion_sig = 200.
        self.mu= init_val
        self.sig = 1000.
    
    def update(self, update_val):
        mean1 = self.mu
        var1 = self.sig 
        mean2 = update_val
        var2 = self.measurement_sig
        # Calculate the new parameters
        self.mu = (var2*mean1 + var1*mean2)/(var2+var1)
        self.sig = 1/(1/var2 + 1/var1)

        return self.mu

    def predict(self):
        mean1 = self.mu
        var1 = self.sig
        mean2 = 0 # replace with actual motion
        var2 = self.motion_sig
        # Calculate the new parameters
        self.mu = mean1 + mean2
        self.sig = var1 + var2
        return self.mu