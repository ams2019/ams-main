#!/usr/bin/env python
from VelocityEstimator import VelocityEstimator
import math
import rospy
import tf
from std_msgs.msg import Int8
from rosflight_msgs.msg import Command, RCRaw
from sensor_msgs.msg import Imu, Range, LaserScan
from nav_msgs.msg import Path
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Quaternion, Point, Twist, PoseStamped
from decimal import Decimal



class DroneParams:
    # control params: modify drone behaviour
    keep_rot_x = True
    keep_rot_y = True
    keep_rot_z = True

    # state params: get drone state
    ## orientation
    rotation_x = 0
    rotation_y = 0
    rotation_z = 0
    ## position
    height = 0
    x = 0
    y = 0
    ## velocity
    vel_height = 0
    ## acceleration
    acc_x = 0
    acc_y = 0
    acc_z = 0
    ## distances
    ranges = []

from enum import Enum
class FCState(Enum):
    IDLE = 0
    TAKE_OFF = 1
    LANDING = 2

# X = roll
# Y = pitch
# Z = yaw

# drone params
drone_params = DroneParams()

# velocity estimators
vel_height_estimator = VelocityEstimator(0,0.1)


FLIGHT_CONTROL_STATE = "idle"

# Target Points
targetList = [[0,0]]
targetIndex = 0

def getClosestPoint(x,y):
    global targetList, targetIndex
    tmpMinDist = math.hypot(x-targetList[targetIndex][0], y-targetList[targetIndex][1])
    tmpMinIndex = targetIndex
    i = targetIndex+1
    while i < len(targetList):
        dist = math.hypot(x-targetList[i][0], y-targetList[i][1])
        if dist < tmpMinDist:
            tmpMinDist = dist
            tmpMinIndex = i
        i += 1
    return tmpMinIndex

def followPoint(tolerance,current_x, current_y, target_x, target_y, vel_ref_x, vel_ref_y):
    """ Set command message for following a point"""
    global drone_params

    # position reached?
    x_correct = False
    y_correct = False 
    
    # x direction
    if abs(current_x-vel_ref_x) < 0.3:
        velocity_x = 0.002
    elif abs(current_x-vel_ref_x) < 0.4:
        velocity_x = 0.003
    elif abs(current_x-vel_ref_x) < 0.5:
        velocity_x = 0.004
    else:
        velocity_x = 0.005

    if current_x > target_x + tolerance:
        target_vel = velocity_x
    elif current_x < target_x-tolerance:
        target_vel = -velocity_x
    else:
        x_correct = True
        target_vel = 0
    
    if drone_params.acc_x > target_vel:
        msg.y = -0.05
    elif drone_params.acc_x < target_vel:
        msg.y = 0.05
    else:
        msg.y = 0
        
    # y direction
    if abs(current_y-vel_ref_y) < 0.3:
        velocity_y = 0.002
    elif abs(current_y-vel_ref_y) < 0.4:
        velocity_y = 0.003
    elif abs(current_y-vel_ref_y) < 0.5:
        velocity_y = 0.004
    else:
        velocity_y = 0.005

    if current_y > target_y+tolerance:
        target_vel = -velocity_y
    elif current_y < target_y-tolerance:
        target_vel = velocity_y
    else:
        y_correct = True
        target_vel = 0

    if drone_params.acc_y < target_vel:
        msg.x = -0.05
    elif drone_params.acc_y > target_vel:
        msg.x = 0.05
    else:
        msg.x = 0
    
    return x_correct and y_correct

msg = Command()

def autonomous_flight_node():
    global drone_params, targetList, targetIndex, vel_height_estimator
   

    rospy.init_node("main_control", anonymous=True)

    # initialize timers for estimators
    vel_height_estimator.init()

    drone_params = DroneParams()

    # Sensors
    rospy.Subscriber("imu/data", Imu, process_imu)
    sonar_topic_param = rospy.get_param('~sonarTopic', 'sonar')
    rospy.Subscriber(sonar_topic_param, Range, process_sonar)

    # Control
    rospy.Subscriber("/move_base/TrajectoryPlannerROS/global_plan", Path, process_path)
    rospy.Subscriber("main/control", Int8, process_control)
    rospy.Subscriber("main/target", Point, process_target)
    
    # published topics
    pub = rospy.Publisher("command", Command, queue_size=10)

    msg.mode = Command.MODE_ROLL_PITCH_YAWRATE_THROTTLE
    msg.ignore = Command.IGNORE_NONE


    listenerTmp = tf.TransformListener()

    rate = rospy.Rate(20) 
     
    # publish command 
    while not rospy.is_shutdown():
        try:
            # # # # # Obtain drone position from TF # # # # #
            (trans,rot) = listenerTmp.lookupTransform('/map', '/base_footprint', rospy.Time(0))
            drone_params.x = trans[0]
            drone_params.y = trans[1]
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        # # # # # PATH Following # # # # # 
        tolerance = 0.1 
        targetIndex = getClosestPoint(trans[0], trans[1])

        if targetIndex + 10 < len(targetList):
            refIndex = targetIndex + 10
        else:
            refIndex = len(targetList)-1

        pointReached = followPoint(tolerance, trans[0],trans[1], targetList[targetIndex][0], targetList[targetIndex][1], targetList[refIndex][0],targetList[refIndex][1])

        # select next point
        if pointReached: 
            while targetIndex < len(targetList)-1 and abs(targetList[targetIndex][0]-trans[0]) < 2*tolerance and abs(targetList[targetIndex][1]-trans[1]) < 2*tolerance:
                targetIndex += 1

        # PUBLISH /command message
        msg.header.stamp = rospy.Time.now()
        pub.publish(msg)
        rate.sleep()


def process_imu(data):
    global drone_params
    quaternion = data.orientation
    euler = euler_from_quaternion([quaternion.x, quaternion.y, quaternion.z, quaternion.w])
    drone_params.rotation_x = Decimal(euler[0]).quantize(Decimal("1.000"))
    drone_params.rotation_y = Decimal(euler[1]).quantize(Decimal("1.000"))
    drone_params.rotation_z = Decimal(euler[2]).quantize(Decimal("1.000"))

    # # # # # Prevent Drone Rotation (yaw) # # # # # 
    if drone_params.rotation_z < 0:
        msg.z = 0.2
    elif drone_params.rotation_z > 0:
        msg.z = -0.2
    else:
        msg.z = 0

    drone_params.acc_x = data.linear_acceleration.x
    drone_params.acc_y = data.linear_acceleration.y
    drone_params.acc_z = data.linear_acceleration.z

def process_sonar(data):
    global drone_params, vel_height_estimator, FLIGHT_CONTROL_STATE
    drone_params.height = data.range
    drone_params.vel_height = vel_height_estimator.update(drone_params.height)
    
    if FLIGHT_CONTROL_STATE == FCState.TAKE_OFF:
        if drone_params.height < 1 and drone_params.vel_height < 0.001:
            msg.F = 0.7
        else: 
            msg.F = 0.6
    elif FLIGHT_CONTROL_STATE == FCState.LANDING :
        msg.F = 0

def process_path(data):
    global drone_params, targetList, targetIndex
    path = Path()
    path = data
    if targetIndex == len(targetList)-1:
        targetList = []
        targetIndex = 0
        tmpI = 0
        while tmpI < len(path.poses):
            pose = path.poses[tmpI]
            targetList.append([pose.pose.position.x,pose.pose.position.y])
            tmpI += 5
        pose = path.poses[len(path.poses)-1]
        targetList.append([pose.pose.position.x,pose.pose.position.y])   
        targetIndex = getClosestPoint(drone_params.x, drone_params.y)

def process_control(data):
    global FLIGHT_CONTROL_STATE
    if data.data == 0:
        FLIGHT_CONTROL_STATE = FCState.IDLE
    elif data.data == 1:
        FLIGHT_CONTROL_STATE = FCState.TAKE_OFF
    elif data.data == 2:
        FLIGHT_CONTROL_STATE = FCState.LANDING

def process_target(data):
    global targetList, targetIndex
    if targetIndex == len(targetList)-1:
        print("new target")
        targetIndex = 0
        targetList = [[data.x, data.y]]

if __name__ == '__main__':
    try:
        # Collect events until released
        autonomous_flight_node()

    except rospy.ROSInterruptException:
        rospy.loginfo("exception")
