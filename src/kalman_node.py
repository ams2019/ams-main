#!/usr/bin/env python

import rospy
from KalmanFilter import KalmanFilter
import tf
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Quaternion, Point, Twist, PoseStamped

def kalman_node():
   
    rospy.init_node("kalman_node", anonymous=True)

    listenerTmp = tf.TransformListener()

    rate = rospy.Rate(20) 

    posePub = rospy.Publisher("pose_est", PoseStamped, queue_size=10)

    # Kalman init val
    kalmanX = KalmanFilter(0.)
    kalmanY = KalmanFilter(0.)
    br = tf.TransformBroadcaster()
     
    # publish command 
    while not rospy.is_shutdown():
        try:
            (trans,rot) = listenerTmp.lookupTransform('/map', '/scanmatcher_frame', rospy.Time(0))

            kalmanX.update(trans[0])
            kalmanY.update(trans[1])

            poseMsg = PoseStamped()
            poseMsg.header.stamp = rospy.Time()
            poseMsg.header.frame_id = "map"
            poseMsg.pose.position.x = kalmanX.predict()
            poseMsg.pose.position.y = kalmanY.predict()
            poseMsg.pose.position.z = 0
            posePub.publish(poseMsg)
            br.sendTransform((poseMsg.pose.position.x, poseMsg.pose.position.y, 0),
                        rot,
                        rospy.Time.now(),
                        "base_footprint",
                        "map")
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
        rate.sleep()

   
if __name__ == '__main__':
    try:
        kalman_node()

    except rospy.ROSInterruptException:
        rospy.loginfo("exception")
