#!/usr/bin/env python
from threading import Thread
from pynput.keyboard import Key, Listener, Controller # --> pip install pynput
import math

globalShutdown = False

throttlePressed = False
xCtrlPressed = False
yCtrlPressed = False

def on_press(key):
    global throttlePressed, xCtrlPressed, yCtrlPressed
    # yaw with left and right
    try:
        if key.char == 't':
            throttlePressed = True
            msg.F += 0.01
            msg.F = min(msg.F, 1)
            print('t pressed')
    except AttributeError:
        if key == Key.left:
            yCtrlPressed = True
            msg.y = 0.2      
        elif key == Key.right:
            yCtrlPressed = True
            msg.y = -0.2
        # pitch with up and down
        elif key == Key.up:
            xCtrlPressed = True
            msg.x = -0.2
        elif key == Key.down:
            xCtrlPressed = True
            msg.x = 0.2
        # Throttle with space and control
        elif key == Key.ctrl:
            msg.F -= 0.01
            msg.F = max(msg.F, 0)

def on_release(key):
    global globalShutdown, throttlePressed, xCtrlPressed, yCtrlPressed
    try:
        if key.char == "t":
            throttlePressed = False
    except AttributeError:
        if key == Key.left or key == Key.right:
            yCtrlPressed = False
        if key == Key.up or key == Key.down:
            xCtrlPressed = False
        if key == Key.esc:
            # Stop listener
            globalShutdown = True
            return False

def start_key_listener():
    with Listener(
                on_press=on_press,
                on_release=on_release) as listener:
            listener.join()



import rospy
from std_msgs.msg import String
from rosflight_msgs.msg import Command, RCRaw
from sensor_msgs.msg import Imu, Range, LaserScan
from gazebo_msgs.msg import ModelStates
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Quaternion, Point
from decimal import Decimal

msg = Command()
keyboard = Controller()

class VelocityEstimator: # must be initialized after rospy.init_node
    def __init__(self, init_state, interval):
        self.last_state = init_state
        self.interval = interval
        self.value = 0

    def init(self): # init timer
        self.last_time = rospy.get_time() 

    def update(self, update_state):
        now = rospy.get_time()
        diff = now-self.last_time
        if diff >= self.interval:
            self.value = (update_state-self.last_state)/diff
            self.last_time = now
            self.last_state = update_state

        return self.value

class DroneParams:
    # control params: modify drone behaviour
    keep_rot_x = True
    keep_rot_y = True
    keep_rot_z = True

    # state params: get drone state
    ## orientation
    init_rotation_z = -1000
    rotation_x = 0
    rotation_y = 0
    rotation_z = 0
    ## position
    height = 0
    ## velocity
    vel_height = 0
    ## acceleration
    acc_x = 0
    acc_y = 0
    acc_z = 0
    ## distances
    ranges = []



# X = roll
# Y = pitch
# Z = yaw

# drone params
drone_params = DroneParams()

# velocity estimators
vel_height_estimator = VelocityEstimator(0,0.1)
vel_ranges_estimators = []

def main_control():
    global globalShutdown, throttlePressed, vel_height_estimator 
   

    rospy.init_node("main_control", anonymous=True)

    # initialize timers for estimators
    vel_height_estimator.init()

    drone_params = DroneParams()

    rospy.Subscriber("imu/data", Imu, process_imu)
    rospy.Subscriber("sonar", Range, process_sonar)
    rospy.Subscriber("scan", LaserScan, process_lidar)

    rospy.Subscriber("main/target", Point, process_target)

    pub = rospy.Publisher("command", Command, queue_size=10)

    msg.mode = Command.MODE_ROLL_PITCH_YAWRATE_THROTTLE
    msg.ignore = Command.IGNORE_NONE


    rate = rospy.Rate(60) 

    # publish command 
    while not rospy.is_shutdown():
        if globalShutdown:
            break

        # PUBLISH /command message
        msg.header.stamp = rospy.Time.now()
        pub.publish(msg)
        rate.sleep()

    # stop keyboard listener    
    keyboard.press(Key.esc)
    keyboard.release(Key.esc)
    

def lerp(a, b, t):
    if a > b:
        c = a
        a = b
        b = c
    return (t*a) + ((1-t)*b)


init_height = -1000
drone_height = 0
last_time_xy = 0
last_time_z = 0
last_x = -1
last_y = -1
last_z = -1
drone_velocity = {}
drone_velocity["x"] = 0
drone_velocity["y"] = 0
drone_velocity["z"] = 0

target = {}
target["x"] = 0
target["y"] = 0
target["z"] = 0



def process_imu(data):
    global drone_params, xCtrlPressed, yCtrlPressed
    quaternion = data.orientation
    euler = euler_from_quaternion([quaternion.x, quaternion.y, quaternion.z, quaternion.w])
    drone_params.rotation_x = Decimal(euler[0]).quantize(Decimal("1.000"))
    drone_params.rotation_y = Decimal(euler[1]).quantize(Decimal("1.000"))
    drone_params.rotation_z = Decimal(euler[2]).quantize(Decimal("1.000"))
    
    if drone_params.init_rotation_z == -1000:
        drone_params.init_rotation_z = drone_params.rotation_z

    # keep orientation
    if drone_params.keep_rot_x and not xCtrlPressed:
        if drone_params.rotation_x > 0:
                msg.x = -0.015
        elif drone_params.rotation_x < -0:
            msg.x = 0.015
        else:
            msg.x = 0

    if drone_params.keep_rot_y and not yCtrlPressed:
        if drone_params.rotation_y > 0.0:
                msg.y = -0.015
        elif drone_params.rotation_y < 0:
            msg.y = 0.015
        else:
            msg.y = 0

    if drone_params.keep_rot_z:
        if drone_params.rotation_z < drone_params.rotation_z:
            msg.z = 0.1
        elif drone_params.rotation_z < drone_params.rotation_z:
            msg.z = -0.1
        else:
            msg.z = 0

    drone_params.acc_x = data.linear_acceleration.x
    drone_params.acc_y = data.linear_acceleration.y
    drone_params.acc_z = data.linear_acceleration.z

def process_sonar(data):
    global drone_params, vel_height_estimator
    drone_params.height = data.range
    drone_params.vel_height = vel_height_estimator.update(drone_params.height)
    
    if drone_params.height < 1 and drone_params.vel_height < 0.001:
        msg.F = 0.67
    else: 
        msg.F = 0.6

def process_lidar(data):
    global vel_ranges_estimators, drone_params
    angle_min = data.angle_min
    angle_max = data.angle_max
    angle_increment = data.angle_increment
    range_min = data.range_min
    range_max = data.range_max
    scan_time = data.scan_time
    
    if len(vel_ranges_estimators) == 0: # initialize estimators
        for i in range(len(data.ranges)):
            vel_ranges_estimators.append(VelocityEstimator(data.ranges[i], 0.1))
            vel_ranges_estimators[i].init()

    min_range = 1000
    min_range_id = -1
    for i in range(len(data.ranges)):
        if data.ranges[i] > range_min and data.ranges[i] < range_max:
            vel_ranges_estimators[i].update(data.ranges[i])
            if data.ranges[i] < min_range:
                min_range = data.ranges[i]
                min_range_id = i
    
    min_range_deg = math.degrees(angle_min + min_range_id * angle_increment)   
    targetPoint = LaserScan()
    targetPoint = data
    targetPoint.angle_min =  angle_min + min_range_id * angle_increment
    targetPoint.angle_max =  angle_min + min_range_id * angle_increment
    targetPoint.angle_increment =  0
    print(vel_ranges_estimators[min_range_id].value)
    '''if min_range < 2 and vel_ranges_estimators[min_range_id].value < 0:
        if min_range_deg > -45 and min_range_deg < 45 :
            drone_params.keep_rot_y = False
            print("right")
            msg.y = 0.2
        elif min_range_deg > 135 or min_range_deg < -135 :
            drone_params.keep_rot_y = False
            print("left")
            msg.y = -0.2
        else:
            print(min_range_deg)
            drone_params.keep_rot_y = True
        if min_range_deg > 45 and min_range_deg < 135 :
            drone_params.keep_rot_x = False
            print("top")
            msg.x = 0.2   
        elif min_range_deg < -45 and min_range_deg > -135 :
            drone_params.keep_rot_x = False
            print("bottom")
            msg.x = -0.2  
        else:
            drone_params.keep_rot_x = True
    else:
        drone_params.keep_rot_x = True
        drone_params.keep_rot_y = True


    rospy.Publisher("targetPoint", LaserScan, queue_size=10).publish(targetPoint)
    '''
    

def process_target(data):
    global target
    target["x"] = data.x
    target["y"] = data.y
    target["z"] = data.z
    print("new target", target)

if __name__ == '__main__':
    try:
        # Collect events until released
        globalShutdown = False
        t = Thread(target=start_key_listener, args=())
        t.start()
        main_control()

    except rospy.ROSInterruptException:
        rospy.loginfo("exception")
