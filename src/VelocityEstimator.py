#!/usr/bin/env python
import rospy 

class VelocityEstimator: # must be initialized after rospy.init_node
    def __init__(self, init_state, interval):
        self.last_state = init_state
        self.interval = interval
        self.value = 0

    def init(self): # init timer
        self.last_time = rospy.get_time() 

    def update(self, update_state):
        now = rospy.get_time()
        diff = now-self.last_time
        if diff >= self.interval:
            self.value = (update_state-self.last_state)/diff
            self.last_time = now
            self.last_state = update_state

        return self.value